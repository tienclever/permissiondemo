package com.example.r_permission

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

fun Activity.checkRuntimePermissionsStatus(permissions: List<String>): List<PermissionStatus> =
    permissions.map { permission ->
        if (isPermissionGranted(permission)) {
            return@map PermissionStatus.Granted(permission)
        }
        if (shouldShowRequestPermissionRationale(this, permission)) {
            PermissionStatus.Denied.ShouldShowRationale(permission)
        } else {
            PermissionStatus.Denied.Permanently(permission)
        }
    }

fun Context.isPermissionGranted(permission: String): Boolean =
    ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

fun List<PermissionStatus>.allGranted(): Boolean = all { it is PermissionStatus.Granted }

fun List<PermissionStatus>.doNotAskAgain(): Boolean =
    any { it is PermissionStatus.Denied.Permanently }

fun Activity.openAppSetting() {
    startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
        data = Uri.fromParts("package", packageName, null)
    })
}

fun Fragment.openAppSetting() {
    requireActivity().openAppSetting()
}