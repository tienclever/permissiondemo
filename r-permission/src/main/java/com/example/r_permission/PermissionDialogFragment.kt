package com.example.r_permission

import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

class PermissionDialogFragment : DialogFragment() {
    private var mListener: IPermissionListener? = null
    private var permissions: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
    }

    private fun initData() {
        permissions = arguments?.getStringArrayList(PERMISSION)
        when {
            permissions.isNullOrEmpty() -> {
                dismiss()
            }
            permissions?.size == 1 -> {
                setLauncherSingle()
            }
            else -> {
                setLauncherMultiple()
            }
        }
    }

    private fun setLauncherSingle() {
        val permission = permissions?.firstOrNull() ?: return
        val contract = ActivityResultContracts.RequestPermission()
        val launcher = registerForActivityResult(contract) { isGranted ->
            if (isGranted) {
                mListener?.onGranted()
            } else {
                val doNotAskAgain = !shouldShowRequestPermissionRationale(permission)
                mListener?.onDenied(doNotAskAgain)
            }
            dismiss()
        }
        launcher.launch(permission)
    }

    private fun setLauncherMultiple() {
        val contract = ActivityResultContracts.RequestMultiplePermissions()
        val activity = requireActivity()
        val launcher = registerForActivityResult(contract) { mapPermission ->
            val permissionStatus =
                activity.checkRuntimePermissionsStatus(mapPermission.keys.toList())
            if (permissionStatus.allGranted()) {
                mListener?.onGranted()
            } else {
                mListener?.onDenied(permissionStatus.doNotAskAgain())
            }
            dismiss()
        }
        permissions?.toTypedArray()?.let { launcher.launch(it) }
    }

    fun setListener(listener: IPermissionListener): PermissionDialogFragment {
        mListener = listener
        return this
    }

    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, getTagFragment())
    }

    private fun getTagFragment(): String {
        return PermissionDialogFragment::class.java.canonicalName
            ?: PermissionDialogFragment::class.java.simpleName
    }

    override fun onStart() {
        super.onStart()
        removeBackground()
    }

    private fun removeBackground() {
        dialog?.window?.setDimAmount(0f)
    }

    companion object {
        const val PERMISSION = "PERMISSION"

        @JvmStatic
        fun newInstance(vararg permission: String): PermissionDialogFragment {
            return PermissionDialogFragment().apply {
                arguments = bundleOf(PERMISSION to ArrayList(permission.toList()))
            }
        }
    }
}