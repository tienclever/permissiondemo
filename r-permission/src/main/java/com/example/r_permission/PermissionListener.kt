package com.example.r_permission

interface IPermissionListener {
    fun onGranted()
    fun onDenied(doNotAskAgain: Boolean)
}