package com.example.permissiondemo

import android.Manifest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.permissiondemo.databinding.ActivityMainBinding
import com.example.r_permission.IPermissionListener
import com.example.r_permission.PermissionDialogFragment
import com.example.r_permission.openAppSetting

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mAdapter = PermissionAdapter()
        mAdapter.submitList(getListSinglePermission())
        mAdapter.onClickListener = ::requestPermission
        binding.recyclerView.adapter = mAdapter

        binding.btnRequestMultiple.setOnClickListener {
            requestPermission()
        }
    }

    private fun getListSinglePermission() = listOf(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.WRITE_CONTACTS,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.POST_NOTIFICATIONS
    )

    private fun requestPermission(name: String) {
        PermissionDialogFragment
            .newInstance(name)
            .setListener(object : IPermissionListener {
                override fun onGranted() {
                    showToast("isGranted")
                }

                override fun onDenied(doNotAskAgain: Boolean) {
                    showToast("onDenied")
                    if (doNotAskAgain) openAppSetting()
                }
            })
            .show(supportFragmentManager)
    }

    private fun requestPermission() {
        PermissionDialogFragment
            .newInstance(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .setListener(object : IPermissionListener {
                override fun onGranted() {
                    showToast("isGranted")
                }

                override fun onDenied(doNotAskAgain: Boolean) {
                    showToast("onDenied")
                    if (doNotAskAgain) openAppSetting()
                }
            })
            .show(supportFragmentManager)
    }
}