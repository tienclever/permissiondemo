package com.example.permissiondemo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.permissiondemo.databinding.ItemPermissionBinding

class PermissionAdapter : ListAdapter<String, ItemViewHolder>(PermissionDiffUtil()) {
    var onClickListener: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemPermissionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position), onClickListener)
    }
}

class ItemViewHolder(private val binding: ItemPermissionBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(name: String, onClickListener: ((String) -> Unit)? = null) {
        binding.btnName.text = name
        binding.root.setOnClickListener {
            onClickListener?.invoke(name)
        }
        binding.executePendingBindings()
    }
}

class PermissionDiffUtil : DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }
}